module.exports = {
    "postcss": [],
    "svg": {
        "active": true,
        "workflow": "sprite",
        "symbolsConfig": {
            "loadingType": "inject",
            "usePolyfillForExternalSymbols": true,
            "pathToExternalSymbolsFile": ""
        }
    },
    "css": {
        "workflow": "manual"
    },
    "js": {
        "workflow": "modular",
        "bundler": "webpack",
        "lint": false,
        "useBabel": true,
        "removeConsoleLog": true,
        "webpack": {
            "useHMR": false,
            "providePlugin": {
                $: 'jquery',
                jQuery: 'jquery',
                'window.jQuery': 'jquery',
                Popper: 'popper.js',
                Alert: 'exports-loader?Alert!bootstrap/js/src/alert',
                Button: 'exports-loader?Button!bootstrap/js/src/button',
                Carousel: 'exports-loader?Carousel!bootstrap/js/src/carousel',
                Collapse: 'exports-loader?Collapse!bootstrap/js/src/collapse',
                Dropdown: 'exports-loader?Dropdown!bootstrap/js/src/dropdown',
                Modal: 'exports-loader?Modal!bootstrap/js/src/modal',
                Popover: 'exports-loader?Popover!bootstrap/js/src/popover',
                Scrollspy: 'exports-loader?Scrollspy!bootstrap/js/src/scrollspy',
                Tab: 'exports-loader?Tab!bootstrap/js/src/tab',
                Tooltip: "exports-loader?Tooltip!bootstrap/js/src/tooltip",
                Util: 'exports-loader?Util!bootstrap/js/src/util'
            }
        },
        "jsPathsToConcatBeforeModulesJs": [],
        "lintJsCodeBeforeModules": false,
        "jsPathsToConcatAfterModulesJs": [],
        "lintJsCodeAfterModules": false
    },
    "sourcemaps": {
        "js": {
            "active": true,
            "inline": true
        },
        "css": {
            "active": true,
            "inline": true
        }
    },
    "notifyConfig": {
        "useNotify": false,
        "title": "TARS notification",
        "sounds": {},
        "taskFinishedText": "Task finished at: "
    },
    "minifyHtml": false,
    "generateStaticPath": true,
    "buildPath": "./builds/",
    "useBuildVersioning": true,
    "useArchiver": true,
    "ulimit": 4096,
    "templater": "handlebars",
    "cssPreprocessor": "scss",
    "useImagesForDisplayWithDpi": [
        96
    ],
    "fs": {
        "staticFolderName": "static",
        "imagesFolderName": "img",
        "componentsFolderName": "components"
    },
    "staticPrefix": "static/"
};